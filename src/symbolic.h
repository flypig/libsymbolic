/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * 
 * Copyright (c) 2003-2016 David Llewellyn-Jones
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions.
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 */

#if !defined _H_SYMBOLIC
#define _H_SYMBOLIC

//////////////////////////////////////////////////////////////////
// Includes

//#include "local.h"

#if defined _RISC_OS
#include "oslib/types.h"
#include "oslib/os.h"
#include "oslib/macros.h"
#endif

#include <stddef.h>
#if !defined (WIN32)
#include <stdbool.h>
#endif

//////////////////////////////////////////////////////////////////
// Defines

//#define _DEBUG
#define _MEM_PROFILE

#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif

#define CONTFRAC_MAXDEPTH (10)

//////////////////////////////////////////////////////////////////
// Structures

// Main operation structure
typedef struct _Operation Operation;
// Main variable strucutre
typedef struct _Variable Variable;
// Main user function strucutre
typedef struct _UserFunc UserFunc;

// User operation callbacks
typedef double (*UserApproximate)(double fVar1, void * psContext);
typedef Operation * (*UserDifferentiate)(Operation * psOp, Operation * psWRT, void * psContext);
typedef Operation * (*UserSimplify)(Operation * psOp, void * psContext);

// Unary operations
typedef enum _OPUNARY
{
	OPUNARY_INVALID = -1,

	OPUNARY_NEGATIVE,
	OPUNARY_NOT,
	OPUNARY_VC,
	OPUNARY_COSEC,
	OPUNARY_SINH,
	OPUNARY_COSH,
	OPUNARY_TANH,
	OPUNARY_COS,
	OPUNARY_SIN,
	OPUNARY_TAN,
	OPUNARY_SEC,
	OPUNARY_COT,
	OPUNARY_ARCSIN,
	OPUNARY_ARCCOS,
	OPUNARY_ARCTAN,
	OPUNARY_LN,
	OPUNARY_EXP,
	OPUNARY_ABS,

	OPUNARY_NUM
} OPUNARY;

// Textual equivalents of the unary operations
// TODO: This array should be moved into Convert.cpp
static char const aszOpUnary[OPUNARY_NUM][7] = {
	"-",
	"~",
	"VC",
	"cosec",
	"sinh",
	"cosh",
	"tanh",
	"cos",
	"sin",
	"tan",
	"sec",
	"cot",
	"asin",
	"acos",
	"atan",
	"ln",
	"exp",
	"abs"
};

// Binary operations
typedef enum _OPBINARY {
	OPBINARY_INVALID = -1,

	OPBINARY_IND,
	OPBINARY_LAND,
	OPBINARY_LOR,
	OPBINARY_LIMP,
	OPBINARY_POW,
	OPBINARY_ADD,
	OPBINARY_SUB,
	OPBINARY_MUL,
	OPBINARY_DIV,
	OPBINARY_DIVIDE,
	OPBINARY_MOD,
	OPBINARY_OR,
	OPBINARY_AND,
	OPBINARY_BIC,
	OPBINARY_EOR,
	OPBINARY_ROR,
	OPBINARY_ROL,
	OPBINARY_EQ,
	OPBINARY_NE,
	OPBINARY_LT,
	OPBINARY_LE,
	OPBINARY_GT,
	OPBINARY_GE,

	OPBINARY_NUM
} OPBINARY;

// Textual equivalents of the binary operations
// TODO: This array should be moved into Convert.cpp
static char const aszOpBinary[OPBINARY_NUM][4] = {
	"ind",
	"^",
	"v",
	"->",
	"**",
	"+",
	"-",
	"*",
	"DIV",
	"/",
	"MOD",
	"OR",
	"AND",
	"BIC",
	"EOR",
	">>",
	"<<",
	"=",
	"!=",
	"<",
	"<=",
	">",
	">="
};

// Ternary operatons
typedef enum _OPTERNARY {
	OPTERNARY_INVALID = -1,

	OPTERNARY_SET,

	OPTERNARY_NUM
} OPTERNARY;

//////////////////////////////////////////////////////////////////
// Function prototypes

// Memory related functions
void PropMemReset (void);
void PropMemOutput (void);
void * PropMemMalloc (size_t size);
void * PropMemCalloc (size_t n, size_t size);
void PropMemFree (void * ptr);

// Creation operations
Operation * CreateMemory (void);
Operation * CreateInteger (int const nInteger);
Operation * CreateTruthValue (bool const boTruth);
Operation * CreateVariable (char const * szVar);
Operation * CreateUnary (OPUNARY eOpType, Operation * psVar1);
Operation * CreateBinary (OPBINARY eOpType, Operation * psVar1, Operation * psVar2);
Operation * CreateTernary (OPTERNARY eOpType, Operation * psVar1, Operation * psVar2, Operation * psVar3);
Operation * CreateUserUnary (char const * szVar, Operation * psVar1);
Operation * CreateUserUnaryLength (char const * szVar, int nLength, Operation * psVar1);
void SetUserUnaryFunc (Operation * psOp, UserFunc * psUserFunc);

// Conversion to and from strings
char * OperationToString (Operation * psOp, char * szString, int nStrLen);
int OperationToStringLength (Operation * psOp);
char * OperationToStringC (Operation * psOp, char * szString, int nStrLen);
int OperationToStringCLength (Operation * psOp);
Operation * StringToOperation (char const * szString);

// Managing operations
void FreeRecursive (Operation * psOp);
Operation * CopyRecursive (Operation * psOp);
Operation * FindOperation (Operation * psMain, Operation * psFind);

// Manipulating operations mathematically
Operation * SubstituteOperation (Operation * psMain, Operation * psFind, Operation * psSub);
Operation * SubstituteOperationPair (Operation * psMain, Operation * psFind1, Operation * psSub1, Operation * psFind2, Operation * psSub2);
Operation * SimplifyOperation (Operation * psOp);
bool CompareOperations (Operation * psOp1, Operation * psOp2);
Operation * ContinuedFraction (double fDecimal, unsigned int uMaxDepth);
double ApproximateOperation (Operation * psOp);
Operation * UberSimplifyOperation (Operation * psOp);
Operation * DifferentiateOperation (Operation * psOp, Operation * psWRT);

// Managing variables
Variable * CreateVariableValue (Operation * psOp, Variable * psVariables);
Variable * CreateVariables (Operation * psOp, Variable * psVariables);
Variable * FindVariable (Variable * psVariables, char const * const szVar);
void SetVariable (Variable * psVar, double fValue);
double GetVariable (Variable * psVar);
void UnsetVariable (Variable * psVar);
Variable * FreeVariables (Variable * psVariables);
int VariableCount (Variable * psVariables);
Variable * VariableFirst (Variable * psVariables);
Variable * VariableLast (Variable * psVariables);
Variable * VariableNext (Variable * psVariables);
Variable * VariablePrev (Variable * psVariables);
char const * VariableName (Variable const * const psVariable);

// Managing user-defined functions
UserFunc * AddNewUserFunc (UserFunc * psUserFuncs, char const * const szName);
void SetUserFuncCallbacks (UserFunc * psUserFunc, UserApproximate pfApproximate, UserDifferentiate pfDifferentiate, UserSimplify pfSimplify, void * psContext);
int AssignAllUserFuncs (Operation * psOp, UserFunc * psUserFuncs);
UserFunc * FindUserFunc (UserFunc * psUserFuncs, char const * const szName);
UserFunc * FreeUserFuncs (UserFunc * psUserFuncs);
int UserFuncCount (UserFunc * psUserFuncs);
UserFunc * UserFuncFirst (UserFunc * psUserFuncs);
UserFunc * UserFuncLast (UserFunc * psUserFuncs);
UserFunc * UserFuncNext (UserFunc * psUserFunc);
UserFunc * UserFuncPrev (UserFunc * psUserFunc);
char const * UserFuncName (UserFunc const * const psUserFunc);



#endif // if !defined _H_SYMBOLIC
