/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * See symbolic.h, COPYING file or website for licence
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions.
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 * Implements a main function for simple testing of the library.
 *
 */

//////////////////////////////////////////////////////////////////
// Includes

#include "symbolic.h"
#include "symbolic_private.h"

#include <stdio.h>
#include <string.h>
#include <float.h>
#define _USE_MATH_DEFINES
#include <math.h>

//////////////////////////////////////////////////////////////////
// Defines

// The maximum input size allowed
#define INPUT_SIZE (2048)

//////////////////////////////////////////////////////////////////
// Global variables

//////////////////////////////////////////////////////////////////
// Function prototypes

double TestApproximate (double fVar1, void * psContext);
Operation * TestDifferentiate (Operation * psOp, Operation * psWRT, void * psContext);
Operation * TestSimplify (Operation * psOp, void * psContext);

//////////////////////////////////////////////////////////////////
// Main application

/**
 * Main program.
 * This is for testing purposes and isn't part of the libary.
 *
 */
int main (int argc, char * * argv) {
	Operation * psOp;
	Operation * psOp2;
	char szString[512];
	Operation * psSub;
	Operation * psFind;
	Operation * psFind2;
	double fResult;
	Variable * psVars = NULL;
	Variable * psVar = NULL;
	char szInput[INPUT_SIZE];
	char * szRead;
	UserFunc * psFunc = NULL;
	UserFunc * psFuncs = NULL;

	// If we don't do this we get unused variable warnings
	argc = argc;
	argv = argv;

	printf ("Example structure\n");

	// Clear the memory logging status
	PropMemReset ();

	// Test 1: Substitution

	// Create some example operations
	psOp = CreateBinary (OPBINARY_LOR, CreateBinary (OPBINARY_LAND,
		CreateVariable ("m1"), CreateVariable ("m2")),
		CreateBinary (OPBINARY_IND, CreateMemory (), CreateInteger (57)));
	psOp2 = CopyRecursive (psOp);
	psFind = CreateBinary (OPBINARY_LAND, CreateVariable ("m1"),
		CreateVariable ("m2"));
	psFind2 = CreateVariable ("m2");
	psSub = CreateBinary (OPBINARY_SUB, CreateVariable ("m2"),
		CreateVariable ("m2"));
	psSub = SubstituteOperation (psSub, psFind2, psFind);

	// Convert to string and output to console
	OperationToString (psOp2, szString, sizeof(szString));
	printf("Copied is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psOp, szString, sizeof(szString));
	printf("Output is: %s\n", szString);

	// Perform operation substitution
	psOp = SubstituteOperation (psOp, psFind, psSub);
	psOp = SubstituteOperation (psOp, psFind, psSub);

	// Convert to string and output to console
	OperationToString (psFind, szString, sizeof(szString));
	printf("To find is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psSub, szString, sizeof(szString));
	printf("To substitute is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psOp, szString, sizeof(szString));
	printf("Substituted twice is: %s\n", szString);

	// Free everything up
	FreeRecursive (psOp);
	FreeRecursive (psOp2);
	FreeRecursive (psFind);
	FreeRecursive (psFind2);
	FreeRecursive (psSub);

	// Test 2: String conversion, substitution, approximation

	// Convert from string to operation
	psOp = StringToOperation ("((m1 ^ m2) v (M ind 57))");
	// Make a complete copy
	psOp2 = CopyRecursive (psOp);
	// Convert from string to operation
	psFind = StringToOperation ("(m1 ^ m2)");
	// Convert from string to operation
	psFind2 = StringToOperation ("m2");
	// Convert from string to operation
	psSub = StringToOperation ("(m2 - m2)");
	// Perform a substitution
	psSub = SubstituteOperation (psSub, psFind2, psFind);

	// Convert to string and output to console
	OperationToString (psOp2, szString, sizeof(szString));
	printf("Copied is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psOp, szString, sizeof(szString));
	printf("Output is: %s\n", szString);

	// Perform substitutions
	psOp = SubstituteOperation (psOp, psFind, psSub);
	psOp = SubstituteOperation (psOp, psFind, psSub);

	// Convert to string and output to console
	OperationToString (psFind, szString, sizeof(szString));
	printf("To find is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psSub, szString, sizeof(szString));
	printf("To substitute is: %s\n", szString);

	// Convert to string and output to console
	OperationToString (psOp, szString, sizeof(szString));
	printf("Substituted twice is: %s\n", szString);

	// Approximate the result
	fResult = ApproximateOperation (psOp);
	printf("Approximates to: %f\n", fResult);

	// Free everything up
	FreeRecursive (psOp);
	FreeRecursive (psOp2);
	FreeRecursive (psFind);
	FreeRecursive (psFind2);
	FreeRecursive (psSub);

	// Check everything got freed up properly
	PropMemOutput ();

	printf ("\n");

	// Test 3: Approximation, simplification

	printf ("Example formula\n");
	psOp = StringToOperation ("2**(8/(2**2))");
	OperationToString (psOp, szString, sizeof(szString));
	printf("Formula is: %s\n", szString);
	fResult = ApproximateOperation (psOp);
	printf("Approximates to: %f\n", fResult);
	psOp = UberSimplifyOperation (psOp);
	OperationToString (psOp, szString, sizeof(szString));
	printf("Simplified is: %s\n", szString);
	fResult = ApproximateOperation (psOp);
	printf("Approximates to: %f\n", fResult);

	// Free everything up
	FreeRecursive (psOp);

	// Check everything got freed up properly
	PropMemOutput ();

	printf ("\n");

	// Test 4: Continuous fractions, simplification, approximation

	printf ("Example continued fraction\n");
	psOp = ContinuedFraction (0.398472398472, CONTFRAC_MAXDEPTH);
	OperationToString (psOp, szString, sizeof(szString));
	printf("Continued fraction is: %s\n", szString);
	psOp = UberSimplifyOperation (psOp);
	OperationToString (psOp, szString, sizeof(szString));
	printf("Simplified fraction is: %s\n", szString);
	fResult = ApproximateOperation (psOp);
	printf("Approximates to: %f\n", fResult);

	// Free everything up
	FreeRecursive (psOp);

	// Check everything got freed up properly
	PropMemOutput ();

	printf ("\n");

	// Test 5: Differentiation

	printf ("Example derivative\n");
	psOp = StringToOperation ("cos ((x**2) + (y**2)) / ((x**2) + (y**2))");
	//psOp = StringToOperation ("(x**2) + (y**2)");
	OperationToString (psOp, szString, sizeof(szString));
	printf("Formula is: %s\n", szString);

	psVars = CreateVariables (psOp, psVars);
	printf("Number of variables: %d\n", VariableCount (psVars));
	psVar = FindVariable (psVars, "x");
	SetVariable (psVar, 2.0);
	psVar = FindVariable (psVars, "y");
	SetVariable (psVar, 3.0);
	psVar = FindVariable (psVars, "x");
	printf ("Variable x has value %f\n", GetVariable (psVar));
	psVar = FindVariable (psVars, "y");
	printf ("Variable y has value %f\n", GetVariable (psVar));
	fResult = ApproximateOperation (psOp);
	printf ("Approximates to: %f\n", fResult);
	psVars = FreeVariables (psVars);
	printf("Number of variables: %d\n", VariableCount (psVars));

	psSub = StringToOperation ("x");
	OperationToString (psSub, szString, sizeof(szString));
	printf("Differentiate WRT: %s\n", szString);
	psOp2 = DifferentiateOperation (psOp, psSub);
	OperationToString (psOp2, szString, sizeof(szString));
	printf("Derivative is: %s\n", szString);

	psVars = CreateVariables (psOp2, psVars);
	fResult = ApproximateOperation (psOp2);
	printf ("Approximates to: %f\n", fResult);

	psOp2 = UberSimplifyOperation (psOp2);
	OperationToString (psOp2, szString, sizeof(szString));
	printf("Simplified is: %s\n", szString);

	psVars = CreateVariables (psOp2, psVars);
	fResult = ApproximateOperation (psOp2);
	printf ("Approximates to: %f\n", fResult);

	// Free everything up
	FreeRecursive (psSub);
	FreeRecursive (psOp2);
	FreeRecursive (psOp);
	psVars = FreeVariables (psVars);

	// Test 6: User functions
	printf ("\n");
	printf ("Input: %s\n", "test(x + 2) * test (15 + y)");
	psOp = StringToOperation ("test(x + 2) * test (15 + y)");
	OperationToString (psOp, szString, sizeof(szString));
	printf ("Read as: %s\n", szString);
	printf ("List variables\n");
	psVar = VariableFirst (psVars);
	while (psVar) {
		printf ("\t%s = %f\n", VariableName (psVar), GetVariable (psVar));
		psVar = VariableNext (psVar);
	}
	psVars = CreateVariables (psOp, psVars);

	psVar = FindVariable (psVars, "x");
	if (psVar) {
		SetVariable (psVar, 2.0);
	}
	psVar = FindVariable (psVars, "y");
	if (psVar) {
		SetVariable (psVar, 3.0);
	}

	printf ("List variables\n");
	psVar = VariableFirst (psVars);
	while (psVar) {
		printf ("\t%s = %f\n", VariableName (psVar), GetVariable (psVar));
		psVar = VariableNext (psVar);
	}

	fResult = ApproximateOperation (psOp);
	printf ("Approximates to: %f\n", fResult);

	printf ("Assign user functions\n");
	psFuncs = AddNewUserFunc (psFuncs, "test");
	SetUserFuncCallbacks (psFuncs, TestApproximate, TestDifferentiate, TestSimplify, NULL);

	printf ("List functions\n");

	psFunc = UserFuncFirst (psFuncs);
	while (psFunc) {
		printf ("\t%s ()\n", UserFuncName (psFunc));
		psFunc = UserFuncNext (psFunc);
	}
	AssignAllUserFuncs (psOp, psFuncs);

	fResult = ApproximateOperation (psOp);
	printf ("Approximates to: %f\n", fResult);


	// Free everything up
	FreeRecursive (psOp);
	psVars = FreeVariables (psVars);
	psFuncs = FreeUserFuncs (psFuncs);


	// Test 7: Freestyle!

	printf ("\nEnter an expression to perform some calculations on it.\n");
	// Read in data from the user
	fflush (stdout);
	szRead = fgets (szInput, INPUT_SIZE, stdin);
	if (szRead) {
		szInput[(strlen(szInput) - 1)] = 0;
		psOp = StringToOperation (szInput);
		printf ("\nEnter variable to differentiate with respect to.\n");
		fflush (stdout);
		// Read in data from the user
		szRead = fgets (szInput, INPUT_SIZE, stdin);
		if (szRead) {
			szInput[(strlen(szInput) - 1)] = 0;
			psSub = StringToOperation (szInput);
			printf ("\n\nResults:\n");

			// Convert the result
			OperationToString (psOp, szString, sizeof(szString));
			printf ("Interpreted: %s\n", szString);
			// Output it as a C/C++/GLSL string
			OperationToStringC (psOp, szString, sizeof(szString));
			printf ("C String: %s\n", szString);
			// Simplify
			psOp = UberSimplifyOperation (psOp);
			OperationToString (psOp, szString, sizeof(szString));
			printf ("Simplified: %s\n", szString);
			// Differentiate, then simplify the result
			psOp2 = DifferentiateOperation (psOp, psSub);
			psOp2 = UberSimplifyOperation (psOp2);
			OperationToString (psOp2, szString, sizeof(szString));
			printf ("Differentiated: %s\n", szString);
			OperationToStringC (psOp2, szString, sizeof(szString));
			printf ("C String differentiated: %s\n", szString);

			// Free everything up
			FreeRecursive (psSub);
			FreeRecursive (psOp2);
		}

		FreeRecursive (psOp);
	}

	// Final checks
	printf ("\n\nFinal memory checks\n");
	
	psVars = FreeVariables (psVars);
	printf("Number of variables: %d\n", VariableCount (psVars));

	// Check everything got freed up properly
	PropMemOutput ();

	printf ("\n");

	// And relax
	return 0;
}

/**
 * Test user function approximate.
 * This is for testing purposes and isn't part of the libary.
 *
 */
double TestApproximate (double fVar1, void * psContext) {
	return fVar1 * 2;
}

/**
 * Test user function differentiate.
 * This is for testing purposes and isn't part of the libary.
 *
 */
Operation * TestDifferentiate (Operation * psOp, Operation * psWRT, void * psContext) {
	Operation * psReturn = NULL;
	psReturn = CreateInteger (0);

	return psReturn;
}

/**
 * Test user function simplify.
 * This is for testing purposes and isn't part of the libary.
 *
 */
Operation * TestSimplify (Operation * psOp, void * psContext) {
	Operation * psReturn = NULL;
	psReturn = psOp;

	return psReturn;
}

