/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * See symbolic.h, COPYING file or website for licence
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions.
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 * Symbolic has a collection of build in functions that it knows
 * about automatically. Sometimes these may not be enough. The
 * UserUnary functions provide an interface for registering
 * new unary functions defined by the developer, extending the 
 * library's capabilities.
 *
 */

//////////////////////////////////////////////////////////////////
// Includes

#include "symbolic.h"
#include "symbolic_private.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

//////////////////////////////////////////////////////////////////
// Defines

//////////////////////////////////////////////////////////////////
// Structures

// Structure for head of the linked list
typedef struct _UserFuncHead {
	int nNumber;

	UserFunc * psFuncFirst;
	UserFunc * psFuncLast;
} UserFuncHead;

// Linked list of user functions
struct _UserFunc {
	char * szName;
	void * psContext;
	int nReferences;

	UserApproximate pfApproximate;
	UserDifferentiate pfDifferentiate;
	UserSimplify pfSimplify;

	UserFuncHead * psFuncHead;
	UserFunc * psFuncNext;
	UserFunc * psFuncPrev;
};

//////////////////////////////////////////////////////////////////
// Global variables

//////////////////////////////////////////////////////////////////
// Function prototypes

//////////////////////////////////////////////////////////////////
// Main application

/**
 * 
 *
 * @param 
 * @return 
 *
 */
Operation * CreateUserUnary (char const * szVar, Operation * psVar1) {
	Operation * psOp;

	psOp = (Operation*)PropMalloc (sizeof(Operation));
	psOp->eOpType = OPTYPE_UNARYUSER;
	psOp->Vars.psUnaryUser = (OpUnaryUser*)PropMalloc(sizeof(OpUnaryUser));

	psOp->Vars.psUnaryUser->szName = PropMalloc (strlen (szVar) + 1);
	strcpy (psOp->Vars.psUnaryUser->szName, szVar);
	psOp->Vars.psUnaryUser->psVar1 = psVar1;

	psOp->Vars.psUnaryUser->psUserFunc = NULL;

	return psOp;
}

/**
 * 
 *
 * @param 
 * @return 
 *
 */
Operation * CreateUserUnaryLength (char const * szVar, int nLength, Operation * psVar1) {
	Operation * psOp;

	psOp = (Operation*)PropMalloc (sizeof(Operation));
	psOp->eOpType = OPTYPE_UNARYUSER;
	psOp->Vars.psUnaryUser = (OpUnaryUser*)PropMalloc(sizeof(OpUnaryUser));

	psOp->Vars.psUnaryUser->szName = PropMalloc (nLength + 1);
	strncpy (psOp->Vars.psUnaryUser->szName, szVar, nLength);
	psOp->Vars.psUnaryUser->szName[nLength] = 0;

	psOp->Vars.psUnaryUser->psVar1 = psVar1;

	psOp->Vars.psUnaryUser->psUserFunc = NULL;

	return psOp;
}

/**
 * 
 *
 * @param 
 * @return 
 *
 */
void SetUserUnaryFunc (Operation * psOp, UserFunc * psUserFunc) {
	if ((psOp->Vars.psUnaryUser->psUserFunc != psUserFunc) && (psOp->Vars.psUnaryUser->psUserFunc)) {
		DecrementUserFuncRef (psOp->Vars.psUnaryUser->psUserFunc);
		IncrementUserFuncRef (psUserFunc);
	}

	psOp->Vars.psUnaryUser->psUserFunc = psUserFunc;
}

/**
 * For reference counting.
 *
 * @param 
 * @return 
 *
 */
void IncrementUserFuncRef (UserFunc * psUserFunc) {
	psUserFunc->nReferences++;
}

/**
 * For reference counting.
 *
 * @param 
 * @return 
 *
 */
void DecrementUserFuncRef (UserFunc * psUserFunc) {
	psUserFunc->nReferences--;
}

/**
 * Recursively copy a formula and all its subformulas.
 *
 * @param 
 * @return 
 *
 */
Operation * CopyUser (Operation * psOp) {
	Operation * psReturn;

	psReturn = CreateUserUnary (psOp->Vars.psUnaryUser->szName, CopyRecursive (psOp->Vars.psUnaryUser->psVar1));

	SetUserUnaryFunc (psReturn, psOp->Vars.psUnaryUser->psUserFunc);
	if (psReturn->Vars.psUnaryUser->psUserFunc) {
		IncrementUserFuncRef (psReturn->Vars.psUnaryUser->psUserFunc);
	}

	return psReturn;
}

/**
 * Recursively approximate an operation if possible.
 *
 * @param psOp the operation to evaluate recursively.
 * @return the approximated value.
 *
 */
double UserApproximateDefault (Operation * psOp) {
	double fReturn = NAN;
	double fVar1;
	UserFunc * psUserFunc;

	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		psUserFunc = psOp->Vars.psUnaryUser->psUserFunc;
		if (psUserFunc && psUserFunc->pfApproximate) {
			fVar1 = ApproximateOperation (psOp->Vars.psUnary->psVar1);
			fReturn = psUserFunc->pfApproximate (fVar1, psUserFunc->psContext);
		}
		else {
			// Assume default of the constant zero function
			fReturn = 0.0;
		}
	}
	
	// Return the result
	return fReturn;
}

/**
 * 
 * Recursively differentiate an operation if possible
 * The result will be a newly allocated set of structures
 * so should be freed using FreeRecursive once it's no longer needed
 * This function is recursive.
 *
 * @param psOp the operation to differentiate.
 * @param psWRT an operation represnting the variable to differentate with 
 *        respect to.
 * @return a new operation, the differentiated result.
 *
 */
Operation * UserDifferentiateDefault (Operation * psOp, Operation * psWRT) {
	Operation * psReturn = NULL;
	OpBinary * psBin = NULL;
	UserFunc * psUserFunc;

	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		psUserFunc = psOp->Vars.psUnaryUser->psUserFunc;
		if (psUserFunc && psUserFunc->pfDifferentiate) {
			psReturn = psUserFunc->pfDifferentiate (psOp, psWRT, psUserFunc->psContext);
		}
		else {
			// Assume default of the constant zero function
			// Constants differentate to 0
			psReturn = CreateInteger (0);
		}
	}

	return psReturn;
}

/**
 * 
 * Recursively simplify an operation if possible
 * Because a new version may created and the original may be freed
 * it's common practice to overwrite the variable pointing to the
 * input structure with the return value
 *
 * @param psOp the operation to simplify, which may be entirely freed. 
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed
 *
 */
Operation * UserSimplifyDefault (Operation * psOp) {
	Operation * psReturn = NULL;
	OpUnary * psUna = NULL;
	OpBinary * psBin = NULL;
	OpTernary * psTer = NULL;
	Operation * psTemp = NULL;
	UserFunc * psUserFunc;

	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		psUserFunc = psOp->Vars.psUnaryUser->psUserFunc;
		if (psUserFunc && psUserFunc->pfSimplify) {
			psReturn = psUserFunc->pfSimplify (psOp, psUserFunc->psContext);
		}
		else {
			// Assume can't be simplified
			psReturn = psOp;
		}
	}

	return psReturn;
}

/**
 * Compare two formulae recursively.
 *
 * @param 
 * @return 
 *
 */
bool UserCompare (Operation * psOp1, Operation * psOp2) {
	bool boReturn = TRUE;

	if ((psOp1) && (psOp1->eOpType == OPTYPE_UNARYUSER) && (psOp2) && (psOp2->eOpType == OPTYPE_UNARYUSER)) {
		if (strcmp (psOp1->Vars.psUnaryUser->szName, psOp2->Vars.psUnaryUser->szName) != 0) {
			boReturn = FALSE;
		}
		else {
			boReturn = CompareOperations (psOp1->Vars.psUnaryUser->psVar1, psOp2->Vars.psUnaryUser->psVar1);
		}
	}
	else {
		boReturn = FALSE;
	}

	return boReturn;
}

/**
 * Recursively turn a formula into a string
 * Internal method. Directly allocates memory for the result, 
 * which must be freed manually once it's no longer needed
 * using PropFree
 *
 * @param psOp the operation to convert
 * @param nStrLen the maximum length the string can take.
 * @return the resulting string in allocated memory.
 *
 */
char * UserToString (Operation * psOp, char * szString, int nStrLen) {
	char * szVar1;

	// Convert the operations recursively
	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		// Unary operations must be handled recursively
		// First convert the result the unary operation is applied to
		szVar1 = RecurseToString (psOp->Vars.psUnaryUser->psVar1, nStrLen);

		// These operations are of the form fn(a) (function fn applied to a)
		snprintf (szString, nStrLen, "%s(%s)", psOp->Vars.psUnaryUser->szName, szVar1);
		szString[nStrLen - 1] = 0;

		// Free up the allocated string since we've already a copy
		PropFree (szVar1);
	}
	else {
		// The operation is NULL
		strncpy (szString, "", nStrLen);
	}

	return szString;
}

/**
 * Recursively return the length of a formula turned into a string.
 *
 * @param psOp the operation to check.
 * @return the length.
 *
 */
int UserToStringLength (Operation * psOp) {
	int nReturn;
	int nVar1;

	// The string will have zero length unless we determine otherwise
	nReturn = 0;

	// Convert the operations recursively
	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		nVar1 = RecurseToStringLength (psOp->Vars.psUnaryUser->psVar1);
		nReturn = strlen (psOp->Vars.psUnaryUser->szName) + nVar1 + 2;
	}
	else {
		// The operation is NULL
		nReturn = 0;
	}

	return nReturn;
}

/**
 * 
 * Recursively turn a formula into a string using float (0.0f) notation
 * This will output a string that can be compiled into C/C++ or GLSL
 * Internal method. Directly allocates memory for the result, 
 * which must be freed manually once it's no longer needed
 * using PropFree
 *
 * @param psOp the operation to convert.
 * @param nStrLen the maximum length the string can take.
 * @return the resulting string in allocated memory.
 *
 */
char * UserToStringC (Operation * psOp, char * szString, int nStrLen) {
	char * szVar1;

	// Convert the operations recursively
	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		// Unary operations must be handled recursively
		// First convert the result the unary operation is applied to
		szVar1 = RecurseToStringC (psOp->Vars.psUnaryUser->psVar1, nStrLen);

		// These operations are of the form fn(a) (function fn applied to a)
		snprintf (szString, nStrLen, "%s(%s)", psOp->Vars.psUnaryUser->szName, szVar1);
		szString[nStrLen - 1] = 0;

		// Free up the allocated string since we've already a copy
		PropFree (szVar1);
	}
	else {
		// The operation is NULL
		strncpy (szString, "", nStrLen);
	}

	return szString;
}

/**
 * Recursively return the length of a formula turned into a string using float 
 * (0.0f) notation.
 *
 * @param psOp the operation to check.
 * @return the length.
 *
 */
int UserToStringCLength (Operation * psOp) {
	int nReturn;
	int nVar1;

	// The string will have zero length unless we determine otherwise
	nReturn = 0;

	// Convert the operations recursively
	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		nVar1 = RecurseToStringCLength (psOp->Vars.psUnaryUser->psVar1);
		nReturn = strlen (psOp->Vars.psUnaryUser->szName) + nVar1 + 2;
	}
	else {
		// The operation is NULL
		nReturn = 0;
	}

	return nReturn;
}

/**
 * Recursively free up all of the memory used by a formula and its sub formulas.
 *
 * @param 
 * @return 
 *
 */
void UserFreeRecursive (Operation * psOp) {
	if ((psOp) && (psOp->eOpType == OPTYPE_UNARYUSER)) {
		if (psOp->Vars.psUnaryUser->psUserFunc) {
			DecrementUserFuncRef (psOp->Vars.psUnaryUser->psUserFunc);
		}
		FreeRecursive (psOp->Vars.psUnaryUser->psVar1);
		PropFree (psOp->Vars.psUnaryUser->szName);
		PropFree (psOp->Vars.psUnaryUser);
	}
}

/**
 * Add a new nambed user function to the list.
 *
 * @param psUserFuncs the linked list of user functions to check.
 * @param szName the name of the user function to add.
 * @return the structure for the user function created.
 *
 */
UserFunc * AddNewUserFunc (UserFunc * psUserFuncs, char const * const szName) {
	UserFunc * psNewFunc = NULL;
	UserFuncHead * psFuncHead = NULL;
	int nNameLen;
	
	psNewFunc = FindUserFunc (psUserFuncs, szName);
	
	if (psNewFunc == NULL) {
		psNewFunc = (UserFunc *)PropMalloc (sizeof (UserFunc));
		
		// Store the user functioon name
		nNameLen = (int)strlen (szName);
		psNewFunc->szName = (char *)PropMalloc (nNameLen + 1);
		strncpy (psNewFunc->szName, szName, nNameLen);
		psNewFunc->szName[nNameLen] = 0;
		psNewFunc->psContext = NULL;
		
		// Initialise callbacks and counts
		psNewFunc->pfApproximate = NULL;
		psNewFunc->pfDifferentiate = NULL;
		psNewFunc->pfSimplify = NULL;
		psNewFunc->nReferences = 0;

		// Link into the linked list
		if (psUserFuncs) {
			psFuncHead = psUserFuncs->psFuncHead;

			psNewFunc->psFuncHead = psFuncHead;

			psNewFunc->psFuncNext = NULL;
			psNewFunc->psFuncPrev = psFuncHead->psFuncLast;

			psFuncHead->psFuncLast->psFuncNext = psNewFunc;
			psFuncHead->psFuncLast = psNewFunc;
			psFuncHead->nNumber++;
		}
		else {
			psFuncHead = (UserFuncHead *)PropMalloc (sizeof (UserFuncHead));
			psFuncHead->nNumber = 1;
			psFuncHead->psFuncFirst = psNewFunc;
			psFuncHead->psFuncLast = psNewFunc;

			psNewFunc->psFuncHead = psFuncHead;
			psNewFunc->psFuncNext = NULL;
			psNewFunc->psFuncPrev = NULL;
		}
	}

	return psNewFunc;
}

/**
 * Set the function callbacks for the user function.
 *
 * @param psUserFunc the user function to set the callbacks for.
 * @param pfApproximate function returning the evaluated function as a rational
 *        (double) value.
 * @param pfDifferentiate function returning the differential of the function.
 * @param pfSimplify function performing a simplification of the function if 
 *        possible.
 *
 */
void SetUserFuncCallbacks (UserFunc * psUserFunc, UserApproximate pfApproximate, UserDifferentiate pfDifferentiate, UserSimplify pfSimplify, void * psContext) {
	psUserFunc->pfApproximate = pfApproximate;
	psUserFunc->pfDifferentiate = pfDifferentiate;
	psUserFunc->pfSimplify = pfSimplify;
	psUserFunc->psContext = psContext;
}

/**
 * Assign any user functions to an operation.
 *
 * @param psOp the operation to find user functions in.
 * @param psUserFuncs the linked list of user functions to assign to the 
 *        operation.
 * @return the number of user functions already or newly assigned.
 *
 */
int AssignAllUserFuncs (Operation * psOp, UserFunc * psUserFuncs) {
	UserFunc * psUserFuncFound;
	int nAssigned;

	nAssigned = 0;
	if (psOp) {
		switch (psOp->eOpType) {
			case OPTYPE_MEMORY:
			case OPTYPE_INTEGER:
			case OPTYPE_TRUTHVALUE:
			case OPTYPE_VARIABLE:
				// Nothing else to do - backtrack
				break;
			case OPTYPE_UNARY:
				// Check any operations further down the tree
				if (psOp->Vars.psUnary) {
					nAssigned += AssignAllUserFuncs (psOp->Vars.psUnary->psVar1, psUserFuncs);
				}
				// Then backtrack
				break;
			case OPTYPE_UNARYUSER:
				if (psOp->Vars.psUnaryUser) {
					// Check whether it's already set correctly
					if (psOp->Vars.psUnaryUser->psUserFunc) {
						if (strcmp (psOp->Vars.psUnaryUser->szName, psOp->Vars.psUnaryUser->psUserFunc->szName) != 0) {
							// Remove the previous function
							DecrementUserFuncRef (psOp->Vars.psUnaryUser->psUserFunc);
							psOp->Vars.psUnaryUser->psUserFunc = NULL;
						}
						else {
							nAssigned++;
						}
					}
					
					if (psOp->Vars.psUnaryUser->psUserFunc == NULL) {
						// Add the new function
						psUserFuncFound = FindUserFunc (psUserFuncs, psOp->Vars.psUnaryUser->szName);
						if (psUserFuncFound) {
							psOp->Vars.psUnaryUser->psUserFunc = psUserFuncFound;
							IncrementUserFuncRef (psUserFuncFound);
							nAssigned++;
						}
					}

					// Check any operations further down the tree
					nAssigned += AssignAllUserFuncs (psOp->Vars.psUnaryUser->psVar1, psUserFuncs);
				}
				// Then backtrack
				break;
			case OPTYPE_BINARY:
				// Check any operations further down the tree
				if (psOp->Vars.psBinary) {
					nAssigned += AssignAllUserFuncs (psOp->Vars.psBinary->psVar1, psUserFuncs);
					nAssigned += AssignAllUserFuncs (psOp->Vars.psBinary->psVar2, psUserFuncs);
				}
				// Then backtrack
				break;
			case OPTYPE_TERNARY:
				// Check any operations further down the tree
				if (psOp->Vars.psTernary) {
					nAssigned += AssignAllUserFuncs (psOp->Vars.psTernary->psVar1, psUserFuncs);
					nAssigned += AssignAllUserFuncs (psOp->Vars.psTernary->psVar2, psUserFuncs);
					nAssigned += AssignAllUserFuncs (psOp->Vars.psTernary->psVar3, psUserFuncs);
				}
				// Then backtrack
				break;
			default:
				printf("Invalid operation type\n");
				break;
		}
	}
	
	return nAssigned;
}

/**
 * Find a user function with a given name.
 *
 * @param psUserFuncs the linked list of user functions to check.
 * @param szName the name of the user function to find.
 * @return the structure for the user function if found, or NULL otherwise.
 *
 */
UserFunc * FindUserFunc (UserFunc * psUserFuncs, char const * const szName) {
	bool boFound = FALSE;
	UserFunc * psUserFunc = NULL;

	if (psUserFuncs) {
		psUserFunc = psUserFuncs->psFuncHead->psFuncFirst;
	}

	while ((!boFound) && psUserFunc) {
		if (strcmp (psUserFunc->szName, szName) == 0) {
			boFound = TRUE;
		}
		else {
			psUserFunc = psUserFunc->psFuncNext;
		}
	}

	return psUserFunc;
}

/**
 * Free up any user functions that are no longer used in an operation.
 *
 * @param psUserFuncs the linked list of user functions to check.
 *
 */
UserFunc * FreeUserFuncs (UserFunc * psUserFuncs) {
	UserFuncHead * psFuncHead = NULL;
	UserFunc * psUserFunc = NULL;
	UserFunc * psFuncPrev = NULL;

	if (psUserFuncs) {
		psFuncHead = psUserFuncs->psFuncHead;
		psUserFunc = psFuncHead->psFuncLast;

		while (psUserFunc) {
			psFuncPrev = psUserFunc->psFuncPrev;
			if (psUserFunc->nReferences <= 0) {
				// Unlink the user function
				if (psUserFunc->psFuncPrev) {
					psUserFunc->psFuncPrev->psFuncNext = psUserFunc->psFuncNext;
				}
				if (psUserFunc->psFuncNext) {
					psUserFunc->psFuncNext->psFuncPrev = psUserFunc->psFuncPrev;
				}
				if (psFuncHead->psFuncFirst == psUserFunc) {
					psFuncHead->psFuncFirst = psUserFunc->psFuncNext;
				}
				if (psFuncHead->psFuncLast == psUserFunc) {
					psFuncHead->psFuncLast = psUserFunc->psFuncPrev;
				}
				// Delete and free up the user function
				PropFree ((void *)psUserFunc->szName);
				PropFree ((void *)psUserFunc);
				psFuncHead->nNumber--;
			}
			psUserFunc = psFuncPrev;
		}

		psUserFunc = psFuncHead->psFuncFirst;

		if (psFuncHead->nNumber <= 0) {
			// Free up the list header too
			PropFree ((void *)psFuncHead);
		}
	}

	return psUserFunc;
}

/**
 * Return the number of user functions in the linked list.
 *
 * @param psUserFuncs the linked list of user functions to check.
 * @return the number of user functions in the list.
 *
 */
int UserFuncCount (UserFunc * psUserFuncs) {
	int nCount = 0;

	if (psUserFuncs) {
		nCount = psUserFuncs->psFuncHead->nNumber;
	}

	return nCount;
}

/**
 * Return the first user functioon in the linked list.
 *
 * @param psUserFunc the user function to inspect.
 * @return the first user function in the list.
 *
 */
UserFunc * UserFuncFirst (UserFunc * psUserFuncs) {
	UserFunc * psFind = NULL;
	if (psUserFuncs) {
		psFind = psUserFuncs->psFuncHead->psFuncFirst;
	}
	
	return psFind;
}

/**
 * Return the last user functioon in the linked list.
 *
 * @param psUserFunc the user function to inspect.
 * @return the last user function in the list.
 *
 */
UserFunc * UserFuncLast (UserFunc * psUserFuncs) {
	UserFunc * psFind = NULL;
	if (psUserFuncs) {
		psFind = psUserFuncs->psFuncHead->psFuncLast;
	}
	
	return psFind;
}

/**
 * Return the next user functioon in the linked list.
 *
 * @param psUserFunc the user function to inspect.
 * @return the next user function in the list.
 *
 */
UserFunc * UserFuncNext (UserFunc * psUserFunc) {
	UserFunc * psFind = NULL;
	if (psUserFunc) {
		psFind = psUserFunc->psFuncNext;
	}
	
	return psFind;
}

/**
 * Return the previous user functioon in the linked list.
 *
 * @param psUserFunc the user function to inspect.
 * @return the previous user function in the list.
 *
 */
UserFunc * UserFuncPrev (UserFunc * psUserFunc) {
	UserFunc * psFind = NULL;
	if (psUserFunc) {
		psFind = psUserFunc->psFuncPrev;
	}
	
	return psFind;
}

/**
 * Return the user function name.
 *
 * @param psUserFunc the user function to inspect.
 * @return the name of the variable.
 *
 */
char const * UserFuncName (UserFunc const * const psUserFunc) {
	char const * szName = NULL;
	
	if (psUserFunc) {
		szName = psUserFunc->szName;
	}
	
	return szName;
}


