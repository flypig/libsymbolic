/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * See symbolic.h, COPYING file or website for licence
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions.
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 * Provide functions for differentiation of Operations. The 
 * processes actually perform algebraic partial differentiation
 * with respect to a stipulated variable.
 *
 */

//////////////////////////////////////////////////////////////////
// Includes

#include "symbolic.h"
#include "symbolic_private.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

//////////////////////////////////////////////////////////////////
// Defines

//////////////////////////////////////////////////////////////////
// Structures

//////////////////////////////////////////////////////////////////
// Global variables

//////////////////////////////////////////////////////////////////
// Function prototypes

//////////////////////////////////////////////////////////////////
// Main application

/**
 * Recursively differentiate an operation if possible
 * The result will be a newly allocated set of structures
 * so should be freed using FreeRecursive once it's no longer needed
 * This function is recursive.
 *
 * @param psOp the operation to differentiate.
 * @param psWRT an operation represnting the variable to differentate with 
 *        respect to.
 * @return a new operatino, the differentiated result.
 *
 */
Operation * DifferentiateOperation (Operation * psOp, Operation * psWRT)
{
	Operation * psReturn = NULL;
	OpBinary * psBin = NULL;

	// If this is the independent variable, it differentiates to 1
	if (CompareOperations (psOp, psWRT)) {
		psReturn = CreateInteger (1);
	}
	else {
		if (psOp) {
			switch (psOp->eOpType) {
				case OPTYPE_MEMORY:
				case OPTYPE_INTEGER:
				case OPTYPE_TRUTHVALUE:
					// Constants differentate to 0
					psReturn = CreateInteger (0);
					break;
				case OPTYPE_VARIABLE:
					// Any other variables except the independent variable differentiate to 0
					psReturn = CreateInteger (0);
					break;
				case OPTYPE_UNARY:
					// Unary operations have to be handled individually
					switch (psOp->Vars.psUnary->eOpType) {
						case OPUNARY_NEGATIVE:
							// The negative of the differentated parameter
							psReturn = CreateUnary (OPUNARY_NEGATIVE, 
								DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT));
							break;
						case OPUNARY_COSEC:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_SUB, CreateInteger (0), 
								CreateBinary (OPBINARY_MUL, 
								CreateUnary (OPUNARY_COSEC, CopyRecursive (psOp->Vars.psUnary->psVar1)), 
								CreateUnary (OPUNARY_COT, CopyRecursive (psOp->Vars.psUnary->psVar1)))));
							break;
						case OPUNARY_SINH:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateUnary (OPUNARY_COSH, CopyRecursive (psOp->Vars.psUnary->psVar1)));
							break;
						case OPUNARY_COSH:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateUnary (OPUNARY_SINH, CopyRecursive (psOp->Vars.psUnary->psVar1)));
							break;
						case OPUNARY_TANH:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_SUB, CreateInteger (1),
								CreateBinary (OPBINARY_POW, 
								CreateUnary (OPUNARY_SEC, CopyRecursive (psOp->Vars.psUnary->psVar1)), 
								CreateInteger (2))));
							break;
						case OPUNARY_COS:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_SUB, CreateInteger (0), 
								CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateUnary (OPUNARY_SIN, CopyRecursive (psOp->Vars.psUnary->psVar1))));
							break;
						case OPUNARY_SIN:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateUnary (OPUNARY_COS, CopyRecursive (psOp->Vars.psUnary->psVar1)));
							break;
						case OPUNARY_TAN:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_POW, 
								CreateUnary (OPUNARY_SEC, CopyRecursive (psOp->Vars.psUnary->psVar1)), 
								CreateInteger (2)));
							break;
						case OPUNARY_SEC:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_MUL, 
								CreateUnary (OPUNARY_SEC, CopyRecursive (psOp->Vars.psUnary->psVar1)), 
								CreateUnary (OPUNARY_TAN, CopyRecursive (psOp->Vars.psUnary->psVar1))));
							break;
						case OPUNARY_COT:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_SUB, CreateInteger (0),
								CreateBinary (OPBINARY_POW, 
								CreateUnary (OPUNARY_COSEC, CopyRecursive (psOp->Vars.psUnary->psVar1)), 
								CreateInteger (2))));
							break;
						case OPUNARY_ARCSIN:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_DIVIDE, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_POW, 
								CreateBinary (OPBINARY_SUB, CreateInteger (1),
								CreateBinary (OPBINARY_POW, CopyRecursive (psOp->Vars.psUnary->psVar1), CreateInteger (2))),
								CreateBinary (OPBINARY_DIVIDE, CreateInteger (1), CreateInteger (2))));
							break;
						case OPUNARY_ARCCOS:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_SUB, CreateInteger (0),
								CreateBinary (OPBINARY_DIVIDE, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_POW, 
								CreateBinary (OPBINARY_SUB, CreateInteger (1),
								CreateBinary (OPBINARY_POW, CopyRecursive (psOp->Vars.psUnary->psVar1), CreateInteger (2))),
								CreateBinary (OPBINARY_DIVIDE, CreateInteger (1), CreateInteger (2)))));
							break;
						case OPUNARY_ARCTAN:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_DIVIDE, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateBinary (OPBINARY_ADD, CreateInteger (1),
								CreateBinary (OPBINARY_POW, CopyRecursive (psOp->Vars.psUnary->psVar1), CreateInteger (2))));
							break;
						case OPUNARY_LN:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_DIVIDE, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CopyRecursive (psOp->Vars.psUnary->psVar1));
							break;
						case OPUNARY_EXP:
							// Use the chain rule
							psReturn = CreateBinary (OPBINARY_MUL, DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT), 
								CreateUnary (OPUNARY_EXP, CopyRecursive (psOp->Vars.psUnary->psVar1)));
							break;
						case OPUNARY_ABS:
							// In general abs (f(x)) will just be f(x), so the differential will be f'(x) except where the function moves from positive to negative
							psReturn = DifferentiateOperation (psOp->Vars.psUnary->psVar1, psWRT);
							break;
						default:
							// We don't know how to deal with this function
							fprintf (stderr, "Cannot differentiate unary function.\n");
							psReturn = CreateInteger (0);
							break;
					}
					break;
				case OPTYPE_UNARYUSER:
					psReturn = UserDifferentiateDefault (psOp, psWRT);
					break;
				case OPTYPE_BINARY:
					psBin = psOp->Vars.psBinary;
					// We simplify the operations first, since this can be inefficient otherwise
					psBin->psVar1 = SimplifyOperation (psBin->psVar1);
					psBin->psVar2 = SimplifyOperation (psBin->psVar2);
					psReturn = psOp;
					switch (psBin->eOpType) {
						case OPBINARY_POW:
							// Use the standard approach
							psReturn = CreateBinary (OPBINARY_ADD,
								CreateBinary (OPBINARY_MUL, 
								CreateBinary (OPBINARY_MUL, CopyRecursive (psBin->psVar2), 
								CreateBinary (OPBINARY_POW, CopyRecursive (psBin->psVar1), 
								CreateBinary (OPBINARY_SUB, CopyRecursive (psBin->psVar2), 
								CreateInteger (1)))), DifferentiateOperation (psBin->psVar1, psWRT)),
								CreateBinary (OPBINARY_MUL, CreateBinary (OPBINARY_MUL, 
								CreateUnary (OPUNARY_LN, CopyRecursive (psBin->psVar1)), 
								CreateBinary (OPBINARY_POW, CopyRecursive (psBin->psVar1), 
								CopyRecursive (psBin->psVar2))), DifferentiateOperation (psBin->psVar2, psWRT)));
							break;
						case OPBINARY_ADD:
							// Linear
							psReturn = CreateBinary (OPBINARY_ADD, 
								DifferentiateOperation (psBin->psVar1, psWRT), 
								DifferentiateOperation (psBin->psVar2, psWRT));
							break;
						case OPBINARY_SUB:
							// Linear
							psReturn = CreateBinary (OPBINARY_SUB, 
								DifferentiateOperation (psBin->psVar1, psWRT), 
								DifferentiateOperation (psBin->psVar2, psWRT));
							break;
						case OPBINARY_MUL:
							// Product rule
							psReturn = CreateBinary (OPBINARY_ADD, 
								CreateBinary (OPBINARY_MUL, CopyRecursive (psBin->psVar1), 
								DifferentiateOperation (psBin->psVar2, psWRT)),
								CreateBinary (OPBINARY_MUL, DifferentiateOperation (psBin->psVar1, psWRT),
								CopyRecursive (psBin->psVar2)));
							break;
						case OPBINARY_DIVIDE:
							// Quotient rule
							psReturn = CreateBinary (OPBINARY_DIVIDE, 
								CreateBinary (OPBINARY_SUB, 
								CreateBinary (OPBINARY_MUL, CopyRecursive (psBin->psVar2), 
								DifferentiateOperation (psBin->psVar1, psWRT)),
								CreateBinary (OPBINARY_MUL, DifferentiateOperation (psBin->psVar2, psWRT),
								CopyRecursive (psBin->psVar1))),
								CreateBinary (OPBINARY_POW, CopyRecursive (psBin->psVar2), CreateInteger (2)));
							break;
						case OPBINARY_LOR:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_LAND:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
/*
						case OPBINARY_DIV:
							break;
						case OPBINARY_MOD:
							//psReturn = DifferentiateOperation (psBin->psVar1, psWRT);
							break;
						case OPBINARY_BIC:
							break;
						case OPBINARY_EOR:
							break;
						case OPBINARY_ROR:
							break;
						case OPBINARY_ROL:
							break;
*/
						case OPBINARY_EQ:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_NE:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_LT:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_LE:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_GT:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_GE:
							// Not ideal, but as this will return either 0 or 1 we assume a constant
							psReturn = CreateInteger (0);
							break;
						case OPBINARY_IND:
							// We take this to be a constant
							psReturn = CreateInteger (0);
							break;
						default:
							// We don't know how to deal with this function
							fprintf (stderr, "Cannot differentiate binary function.\n");
							psReturn = CreateInteger (0);
							break;
					}
					break;
				case OPTYPE_TERNARY:
					// We don't know how to deal with this function
					fprintf (stderr, "Cannot differentiate ternary function.\n");
					psReturn = CreateInteger (0);
					break;
				default:
					// We don't know how to deal with this function
					fprintf (stderr, "Cannot differentiate function.\n");
					psReturn = CreateInteger (0);
					break;
			}
		}
	}

	return psReturn;
}

